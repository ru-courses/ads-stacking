\documentclass[a4paper]{article}

\usepackage{minted}            %environment for adding code blocks
\usepackage{booktabs}           % nicer tables

%\usepackage{tikz}              % environment for drawing things
\usepackage{float}              % for the 'H' positioning of figures
\usepackage{a4wide}             % To decrease the margins and allow more text on a page.
\usepackage{graphicx}           % To deal with including pictures.
\usepackage{color}              % To add color.
\usepackage{enumerate}          % To provide a little bit more functionality than with 
                                % LaTeX's default enumerate environment.
\usepackage{array}              % To provide a little bit more functionality than with 
                                % LaTeX's default array environment.
\usepackage{listings}           % for the formatting of code blocks
\usepackage[american]{babel}
\usepackage{amsmath,amssymb}    % better equations
\usepackage{amsthm}             % proof environment

\lstset{                        % for the boxing of code blocks
    basicstyle=\footnotesize, 
    frame=single,
    breaklines=true,
    numbers=left,
    tabsize=2,
}

% numbering for theorems and lemmas
\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}

% Short cut for set creation
\newcommand{\set}[1]{\ensuremath{\left\{{#1}\right\}}}
\newcommand{\setbuild}[2]{\ensuremath{\set{{#1}\mid{#2}}}}
% Short cuts for well known mathematical sets
\newcommand{\NN}{\ensuremath{\mathbb{N}}}
\newcommand{\ZZ}{\ensuremath{\mathbb{Z}}}
\newcommand{\QQ}{\ensuremath{\mathbb{Q}}}
\newcommand{\RR}{\ensuremath{\mathbb{R}}}
% Short cuts for calligraphic letters
\newcommand{\CO}{\ensuremath{\mathcal{O}}}
% An the next command gives a shorthand for the power set of a given set.
\newcommand{\sol}[1]{\underline{\underline{#1}}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\title{Algorithms And Data Structures\\Assignment: Stacking}

\author{Bryan Rinders, s1060340} %\\Grzegorz Roguszczak, s1107470}

\begin{document}
%\maketitle 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                      The header                     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{center}
\scshape 
	\large{\textbf{ALGORITHMS \& DATA STRUCTURES}}\\[5mm]
	\large{Project 2: Stacking}\\[5mm]
	\normalsize{Bryan Rinders \hfill \textit{s1060340}}\\
	% \normalsize{Grzegorz Roguszczak \hfill \textit{s1107470}}\\
	\normalsize{Radboud University \hfill \textit{2023-01-09}}	\rule{\textwidth}{0.4pt} \\
	\vspace{0.3\baselineskip}
\end{center}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                   The text contents                 %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\section*{Prefatory Note}
This report does not contain a detailed analysis of to the case $M>0$ due to only having found a possible solution  shortly before the deadline.


\section{Algorithm Explanation}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%% Improve section %%%%%%%%%%%%%%%%%%%%%%%

The case where $M=0$ reduces the problem to simply finding the longest
decreasing subsequence (LDS) of array $n$, with $N$ elements. This can
be solved efficiently by the Patience sort algorithm. Patient sort
finds the longest increasing subsequence (LIS) of an array. By simply
reversing the input array, $n^{-1}$, Patience sort will find the LDS
of the non-reversed array, $n$.

The rules of Patience sort can be explained using the game of Patience
as an analogy (hence the name Patience sort). This analogy will be
used for the remainder of the report. The input array will be
represented by a face down deck of cards (unsorted cards). There are
also piles of face up cards (sorted cards). At The start of the
algorithm all cards are in the deck and hence the are zero piles of
sorted cards.

Now lets define some things. Let $A$ be a new card taken from the deck
and let there be $k$ piles of already sorted cards. Then let $B$ be
the top card of pile $p_i$ with $0 < i \leq k$. The rules then become:

\begin{enumerate}[1.]
\item $A$ can only be placed on top of existing piles or it can become
  a new pile consisting of only $A$.
  \label{rule4}
\item $A$ can only be placed on top of pile $p_i$ if and only if
  $A<B$.
  \label{rule1}
\item If $A$ is placed on top of $p_i$ then for all $0 < j < i$ if
  card $C$ that lies on top of pile $p_j$ then it holds that
  $C \leq A$. In other words $p_i$ is the left most pile that can hold
  $A$ based on rule \ref{rule1}.
  \label{rule2}
\item If for all $0 < j \leq k$ if card $C$ lies on top of pile $p_j$
  and $C \leq A$ then pile $p_{k+1}$ is created which contains only
  card $A$. In other words if all piles fail rule 1 then create a new
  pile of just $A$ to the right of pile $p_k$.
  \label{rule3}
\end{enumerate}

Let a face down deck of cards represent the input array, $n$, of the
algorithm.  The algorithm iteratively turns over 1 card from the face
down deck of cards and places the card on top of one of the piles or
create a new pile, using the rules defined above. To find the pile on
to which the card must be placed a binary search is used. The use of
binary search is made possible by rule \ref{rule2}. After all the cards in the
deck have been placed on a pile, the number of piles is the length of
the LDS.

Note that since the algorithm is only interested in the length of the
LIS it is possible to only keep track of the top card of every pile
(of sorted cards).


\section{Correctness}
For the case $M=0$. To proof the correctness of this algorithm it is
necessary to show that every pile of sorted cards contains exactly one
card that is in the LIS. Lets start by defining two loop invariants:

\begin{enumerate}[1.]
\item Each pile is sorted from high to low (bottom to top).
  \label{invariant1}
\item The top cards of all piles are sorted from low to high (left to
  right).
  \label{invariant2}
\end{enumerate}

\begin{proof}
  Invariant \ref{invariant1}.
  \begin{enumerate}
  \item Before the loop start \verb+piles+ is empty, hence invariant
    \ref{invariant1} trivially holds.
  \item During the loop invariant \ref{invariant1} holds by rule
    \ref{rule1}.
  \item After the loop is finished the invariant holds by rule
    \ref{rule1}.
  \end{enumerate}
\end{proof}

\begin{proof}
  Invariant \ref{invariant2}.
  \begin{enumerate}
  \item Before the loop start \verb+piles+ is empty, hence invariant
    \ref{invariant1} trivially holds.
  \item During the loop invariant \ref{invariant2} holds by rule
    \ref{rule2}.
  \item After the loop is finished the invariant holds by rule
    \ref{rule2}.
  \end{enumerate}
\end{proof}

Next we show a proof for the lemmas: \\
\begin{lemma}
  \label{lem:at_most_one_per_pile}
  Cards in the same pile can never be in the same LIS.
\end{lemma}

\begin{proof}
  Lemma \ref{lem:at_most_one_per_pile}. \\
  Let $A \dots B$ be a pile (from top to bottom). Then for all $B$ in
  the pile it must be that for all $A$ that are
  on top of $B$:
  \begin{enumerate}
  \item $A$ appeared later in the deck then $B$, by rule \ref{rule4}.
    \label{pf:lem-1}
  \item $A<B$, by rule \ref{rule1}.
    \label{pf:lem-2}
  \item From \ref{pf:lem-1} and \ref{pf:lem-2} it can never be that
    $A$ and $B$ are in the same LIS.
  \end{enumerate}
\end{proof}

\begin{lemma}
  \label{lem:at_least_one_per_pile}
  There is at least one card per pile that is in the LIS.
\end{lemma}

\begin{proof}
  Lemma \ref{lem:at_least_one_per_pile}. \\
  Let $A$ be the last placed card on pile $p_i$.
  \begin{enumerate}
  \item If $i = 0$ then $A$ has no left neighbour and therefore must
    be the smallest card encounter thus far in the deck and therefore
    be the beginning an increasing subsequence.
  \item If $i > 1$ then let $B$ be the top card of pile $p_{i-1}$.
    \begin{enumerate}
    \item $B$ was placed before $A$ and hence $B$ appeared before $A$
      in the deck.
    \item By rule, \ref{rule2} $B<A$.
    \item From 1a and 1b, $B$ and $A$ are an increasing subsequence.
    \end{enumerate}
  \item By induction, every card must have left neighbour that
    together form an increasing subsequence, by 2 or must be the
    beginning of a increasing subsequence, by 1.
  \end{enumerate}
\end{proof}

\begin{theorem}
  \label{thm:LIS}
  The number of piles is the length of the LIS.
\end{theorem}

\begin{proof}
  Theorem \ref{thm:LIS}. \\
  By combinging lemma \ref{lem:at_most_one_per_pile} and
  \ref{lem:at_least_one_per_pile} it must be that each pile contains
  exactly 1 card that is in the LIS. Hence the number of piles is the
  length of the LIS.
\end{proof}

In order to find the longest decreasing subsequence with Patience sort
all that required is to reverse the input array. Reversing the input
array reverses every increasing subsequence to a decreasing
subsequence and vice versa, this trivially holds for every array of
distinct integers.


\section{Complexity}
For the case that $M=0$ this section mentions line numbers that refer
to listing \ref{lst:LIS}, which can be found in the Appendix.

Other than the for loop (line 6) and the function call to
\verb+find_index_of+ (line 7) everything has constant complexity.
The loop of line 6 loops over all the elements of \verb+lst+. The
length of \verb+lst+ is $N$. The \verb+find_index_of+ function on line
7 (inside the for loop) performs a binary search on \verb+piles+
(defined on line 4). \verb+piles+ can have at most $N$ element, this
happens when \verb+lst+ is ordered increasingly. \verb+find_index_of+
therefor has $\CO(\log N)$, by definition of binary search.

Therefore, combining the complexity of the for loop (line 6) and the
binary search (line 7), the function
\verb+longest_increasing_subsequence_length+ has complexity
$\CO(N \log N)$, which is the entire algorithm when $M=0$. Hence the
algorithm for the case $M=0$ has complexity $\CO(N \log N)$. \\

\noindent
For the case $M>0$ the complexity is $\CO(MN^2)$.

\subsection{Space Complexity}
The algorithm described thus far keeps tracks of every card that has
been sorted in the \verb+piles+ variable. This is necessary in order
to find the card that make up the LIS. However, the goal of the
algorithm is to find the length of the LIS. This means it is possible
to only keep track of the top card of every pile in
\verb+piles+ without effecting the result of the algorithm. This
reduces the average space complexity to $\Theta(n^{1/2})$ [1].


\section*{Appendix}
\begin{listing}[H]
  \lstinputlisting[language=python, linerange={15-28}]{../src/main.py}
  \caption{Algorithm to find the length of the longest increasing subsequence of an array.}
  \label{lst:LIS}
\end{listing}


\section*{Sources}
\begin{enumerate}[(1)]
\item http://www.cs.princeton.edu/courses/archive/spring13/cos423/lectures/LongestIncreasingSubsequence-2x2.pdf
% \item https://www.youtube.com/watch?v=22s1xxRvy28
\end{enumerate}



\end{document}
