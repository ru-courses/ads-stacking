# AUTHORS:
# Bryan Rinders, s1060340
# Grzegorz Roguszczak, s1107470

# sub sequence of strictly decreasing numbers

def reconstruct_LDS(dp: list, pallets: list):
    reconstructed_dp = set()
    m = max(dp)
    for i, x in enumerate(dp[::-1]):
        if x == m:
            reconstructed_dp.add(pallets[len(pallets)-i-1])
            m -= 1
    # print(reconstructed_dp)
    return reconstructed_dp


def longest_decreasing_subsequence(N: int, pallets_n: list) -> list:
    # find longest decreasing subsequence of pallets_n
    dp_n = [1] * N
    for i in range(1,N):
        for j in range(i):
            if pallets_n[j] > pallets_n[i]:
                dp_n[i] = max(dp_n[i], dp_n[j]+1)
    # print(dp_n)
    return dp_n


def main():
    # read stdin
    N, M = [int(item) for item in input().split()]
    pallets_n = [int(item) for item in input().split()]
    pallets_m = [int(item) for item in input().split()]
    # print(f'N: {N}, M: {M}\npallets_n: {pallets_n}\npallets_m {pallets_m}')

    if M > 0:
        return -1
    else:
        dp_n = longest_decreasing_subsequence(N, pallets_n)
        return max(dp_n)


if __name__ == '__main__':
    res = main()
    print(res)
