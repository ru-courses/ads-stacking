# COURSE: Algorithms & Data Structures
# AUTHORS: Bryan Rinders, s1060340, Radboud University
# DATE: 2023-01-09

"""
Script to find the length of the Longest Decreasing Subsequence of an array
read from stdin, using the Patience sort algorithm (O(n log n)).
"""


###############################################################################
# functions for M = 0
# O(n log n)

def longest_increasing_subsequence_length(lst):
    """ Find the length of the longest increasing subsequnce of LST. """

    piles = []

    for val in lst:
        index = find_index_of(val, piles)

        if index >= len(piles):
            piles.append(val)
        else:
            piles[index] = val

    return len(piles)


def find_index_of(val, lst):
    """ Do a binary search to find the index of the smallest number in LST
    greater tha VAL.

    Assume than LST is ordered in increasing order. """

    l = -1  # left bound
    r = len(lst)  # right bound

    while r-l > 1:
        m = l + (r - l) // 2  # in the middle of left and right bound

        if val <= lst[m]:
            r = m
        else:
            l = m

    return r


###############################################################################
# functions for M > 0
# O(n^3)

def longest_decreasing_subsequence(N: int, pallets_n: list) -> list:
    """ DP solution to find the longest decreasing subsequence of pallets_n. 
    Returns the array with the sequences lengths of all elements. """

    dp_n = [1] * N
    for i in range(1,N):
        for j in range(i):
            if pallets_n[j] > pallets_n[i]:
                dp_n[i] = max(dp_n[i], dp_n[j]+1)

    return dp_n


def longest_decreasing_subsequence_ext(N: int, M: int, pallets_n: list, pallets_m: list) -> list:
    """ DP solution to find the longest decreasing subsequence of pallets_n and
    pallets_m. The idea is to use the same techique as for find the LDS of 1
    array and extend it to 2D.

    Does not work 100% correctly.
    """

    max_pallet_size = 10**9

    dp = [[2] * (N+1)] * (M+1)

    dp[0] = [0] + longest_decreasing_subsequence(N, pallets_n)
    dp_m = longest_decreasing_subsequence(M, pallets_m)
    N += 1
    M += 1
    pallets_n = [max_pallet_size] + pallets_n  # will not influence LDS
    pallets_m = [max_pallet_size] + pallets_m  # will not influence LDS

    for i, lds in enumerate(dp_m):
        dp[i+1][0] = lds

    for k in range(1,M):
        for j in range(1,N):
            dp[k][j] = dp[k-1][j]
            for i in range(j):
                if pallets_n[j] < min(pallets_n[i], pallets_m[k]):
                    dp[k][j] = max(dp[k][j], dp[k][i]+1)

    return dp

###############################################################################


def main():
    N, M = [int(item) for item in input().split()]
    pallets_n = [int(item) for item in input().split()]
    pallets_m = [int(item) for item in input().split()]

    if M > 0:
        print(max(map(max, longest_decreasing_subsequence_ext(N, M, pallets_n, pallets_m))))
    else:
        #reverse pallets_n to find the length of the longest decreasing subsequence
        print(longest_increasing_subsequence_length(pallets_n[::-1]))


if __name__ == '__main__':
    main()
