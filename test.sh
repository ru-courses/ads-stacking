#!/bin/sh

TEST_DIR='./doc'
TEST_CODE_FILE='main.py'

for infile in "${TEST_DIR}"/*.in; do
    res="$(python src/"${TEST_CODE_FILE}" < "$infile")"
    outfile="${TEST_DIR}/$(basename "$infile" .in).ans"
    solution="$(cat "$outfile")"
    if [ "$res" = "$solution" ]; then
        echo "$(basename "$infile") correct"
    else
        echo "$(basename "$infile") incorrect. ${res} / ${solution}"
    fi
done
